package com.brotoryapp.brotory.resource;

import com.brotoryapp.brotory.model.History;
import com.brotoryapp.brotory.model.TopVisit;
import com.brotoryapp.brotory.repository.HistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.awt.print.Book;
import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.*;

@RestController
public class HistoryController {

    @Autowired
    private HistoryRepository historyRepository;

    @PostMapping("/history")
    public String saveHistory(@RequestBody History history){
        historyRepository.save(history);
        return "Added history with id: "+history.getId();
    }

    @GetMapping("/history")
    public List<History> getHistory(){
        return historyRepository.findAll();
    }

    @GetMapping("/dashboard")
    public List<TopVisit> getDashboard(){
        List<History> historyList = historyRepository.findAll();
        List<TopVisit> topVisits = new ArrayList<>();
        List<TopVisit> top10Visits = new ArrayList<>();
        // Kalo data kurang dari 10 atau empty
        if(historyList.size() < 10){
            for (int i = 0; i < historyList.size(); i++){
                System.out.println(historyList.get(i));
            }
        } else {
            for (int i = 0; i < 39; i++){
                String flag = "";
                if (topVisits.isEmpty()){
                    TopVisit topVisit = new TopVisit();
                    topVisit.setUrl(historyList.get(i).getUrl());
                    topVisit.setCount(1);
                    topVisits.add(topVisit);
                }
                for (int j = 0; j < topVisits.size(); j++){
                    if (topVisits.get(j).getUrl().equals(historyList.get(i).getUrl())){
                        topVisits.get(j).setCount(topVisits.get(j).getCount()+1);
                        flag = "stop";
                    }
                }
                if (!flag.equals("stop")){
                    TopVisit topVisit = new TopVisit();
                    topVisit.setUrl(historyList.get(i).getUrl());
                    topVisit.setCount(1);
                    topVisits.add(topVisit);
                }
            }
        }
        topVisits.sort(Comparator.comparing(TopVisit::getCount).reversed());
        for (int i = 0; i < 9; i++){
            top10Visits.add(topVisits.get(i));
        }
        System.out.println(top10Visits);
        return top10Visits;
    }

    @GetMapping("/history/{id}")
    public Optional<History> getHistory(@PathVariable String id){
        return historyRepository.findById(id);
    }

    @GetMapping("/username")
    public List<History> getUsername(@RequestParam(name =  "username") String username){
        return historyRepository.findUsername(username);
    }

    @DeleteMapping("/delete/{id}")
    public String deleteHistory(@PathVariable String id){
        historyRepository.deleteById(id);
        return "History deleted with id: "+id;
    }

    public String readSqlite(String location, String query) throws IOException {
        String jdbcUrl = location;

        try {
            Connection connection = DriverManager.getConnection(jdbcUrl);
            String sql = query;

            Statement statement = connection.createStatement();

            ResultSet resultSet = statement.executeQuery(sql);

            String data = "";

            int totalHistory = 0;

            while(resultSet.next() && totalHistory < 20){
                History history = new History();
                String uniqueID = UUID.randomUUID().toString();
                String username = "billy.theno";
                String date = resultSet.getString("Time");
                String urlTemp = resultSet.getString("url");
                String url = urlTemp.substring(0, urlTemp.indexOf("/", urlTemp.indexOf("|")+9));
                history.setId(uniqueID);
                history.setUsername(username);
                history.setUrl(url);
                history.setDate(date);
                historyRepository.save(history);
                data += url;
                totalHistory++;
            }

            return data;
        } catch (SQLException e){
            e.printStackTrace();
            return "error";
        }
    }

    public String readWithScanner() throws IOException {
        String file = "src/main/resources/history_export.txt";
        Scanner scanner = new Scanner(new File(file));
        String data = "";
        for (int i=0; i<50; i++){
            History history = new History();
            String tempData = scanner.nextLine();
            String uniqueID = UUID.randomUUID().toString();
            String username = "billy.theno";
            String date = tempData.substring(0, tempData.indexOf("|"));
            String url = tempData.substring(tempData.indexOf("|")+1, tempData.indexOf("/", tempData.indexOf("|")+9));
            history.setId(uniqueID);
            history.setUsername(username);
            history.setUrl(url);
            history.setDate(date);
            historyRepository.save(history);
            data += url;
        }
        return data;
    }

    @GetMapping("/history/read")
    public String readHistory() throws IOException {
        String locationChrome = "jdbc:sqlite:/C:\\My Projects\\brotory\\src\\main\\resources\\History";
        String queryChrome = "select datetime(last_visit_time/1000000-11644473600,'unixepoch') AS Time,url from  urls order by last_visit_time desc";

        String locationFirefox = "jdbc:sqlite:/C:\\My Projects\\brotory\\src\\main\\resources\\places.sqlite";
        String queryFirefox = "SELECT url, datetime(visit_date/1000000,'unixepoch') AS Time\n" +
                                "FROM moz_historyvisits, moz_places\n" +
                                "WHERE\n" +
                                "moz_historyvisits.place_id=moz_places.id\n" +
                                "ORDER BY Time DESC";

        String locationEdge = "jdbc:sqlite:/C:\\My Projects\\brotory\\src\\main\\resources\\History_edge";
        String queryEdge = "SELECT urls.url, datetime(visit_time/1000000-11644473600,'unixepoch') AS Time\n" +
                            "FROM urls, visits\n" +
                            "WHERE\n" +
                            "urls.id=visits.url\n" +
                            "ORDER BY Time DESC\n";

        readSqlite(locationChrome, queryChrome);
        readSqlite(locationFirefox, queryFirefox);
        readSqlite(locationEdge, queryEdge);

        return "success";
    }

}
