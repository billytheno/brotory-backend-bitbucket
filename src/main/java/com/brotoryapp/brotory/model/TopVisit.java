package com.brotoryapp.brotory.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class TopVisit {

    private String url;
    private int count;

}
